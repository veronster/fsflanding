//load libraries
express = require("express");
path = require("path");

//create an instance of express
app = express ();


//reg list
var allReg = [];

// create reg 
var createReg = function (reg){
        return ({
                email: reg.email,
                password: reg.password,
                name: reg.name,
                gender: reg.gender,
                date: reg.date,
                address: reg.address,
                country: reg.country,
                contact: reg.contact            
        });

};

//configure routes

app.get("/create-reg", function(req, resp) {

	allReg.push(createReg(req.query));
	console.log("All orders:\n %s", JSON.stringify(allReg));
	resp.status(201).end();

});

app.get("/get-reg", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(allReg);
});

app.use(express.static(path.join(__dirname, "public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components")));

//configure port 
app.set("port", parseInt(process.argv[2] || process.env.APP_PORT || 3000));

//start up server 
app.listen(app.get("port"), function(){
    console.log("Server started on port %s %d", new Date(), app.get("port"))
});