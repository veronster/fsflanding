// registraton form
(function (){
    var RegApp = angular.module("RegApp",["Reg"]);

       // initialize Registration object
        initForm = function(ctrl) {
            ctrl.email = ""; 
            ctrl.password = "";
            ctrl.name = "";
            ctrl.gender = "";
            ctrl.date = "";
            ctrl.address = "";
            ctrl.country = "";
            ctrl.contact = "";
        };

        // var validateForm = function (ctrl){
            
        //     //validate password

        //     //validate date 

        //     //validate age 

        //     //validate contact number 
        // }

        // create Registration object 
        var createRegObject = function(ctrl){
            return ({
                email: ctrl.email,
                password: ctrl.password,
                name: ctrl.name,
                gender: ctrl.gender,
                date: ctrl.date,
                address: ctrl.address,
                country: ctrl.country,
                contact: ctrl.contact
            })

        };

    // create an instance of the Angular module
    function RegCtrl(RegSvc){
        // save pointer 
        var regCtrl = this;
        
        // RegCtrl.dateStr = ""

        // regCtrl.bday = ""
        // regCtrl.bmonth = "";
        // regCtrl.byear = "";

        regCtrl.statusMsg = "";
        regCtrl.successMsg = false;

        regCtrl.countryList = [
                        {id: "1", ctyname: "Singapore"},
                        {id: "2", ctyname: "Maldives"},
                        {id: "3", ctyname: "Japan"},
                        {id: "4", ctyname: "New Zealand"},
                        {id: "1", ctyname: "Hungary"}];

        initForm(regCtrl);

    //    dateStr = dateStr.concat(regCtrl.bday + "/" + regCtrl.bmonth + "/" + regCtrl.byear);
    //    console.log(dateStr);

    //     validateForm(regCtrl);

        regCtrl.regUser = function (){
            RegSvc.createReg(createRegObject(regCtrl))
                .then(function(){
                    console.log("registration successful....");
                    regCtrl.statusMsg = "Registration successful!";
                    regCtrl.successMsg = true;
                    initForm(regCtrl);
                }).catch(function(){
                    regCtrl.statusMsg = "Something went wrong!";
                });
        };
    };

    RegApp.controller("RegCtrl", ["RegSvc", RegCtrl]);

})();