(function (){
    var Reg = angular.module("Reg",[]);

    var RegSvc = function ($http) {

        var regSvc = this; 

        regSvc.createReg = function(regDetail){
            return($http.get("/create-reg",{
                params: regDetail
            }));
        };

        regSvc.getReg = function(){
            return($http.get("/get-reg")
                .then(function(result){
                    return(result.data);
                }));
        };

    };
    
    // define service
    Reg.service("RegSvc",["$http",RegSvc]);

})();
