(function() {

    var ViewRegApp = angular.module("ViewRegApp",["Reg"]);

    var ViewRegCtrl = function(RegSvc){
        var viewRegCtrl = this;

        viewRegCtrl.reg = [];

        viewRegCtrl.getReg = function(){

                RegSvc.getReg()
                    .then(function(allReg){
                        viewRegCtrl.reg = allReg;
                    });
        };
    };

// register controller

    ViewRegApp.controller = ("ViewRegCtrl",["RegSvc",ViewRegCtrl]);

})();